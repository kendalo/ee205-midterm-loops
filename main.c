///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
   long suma =0;
   long sumb =0;
   long sumc =0;
   // Sum array1[] with a for() loop and print the result
for(int i=0;i<100;i++){
   
   suma = suma+threeArrays.array1[i];

}

   // Sum array2[] with a while() { } loop and print the result
int x =0;
while(x<100){
   sumb = sumb + threeArrays.array2[x];
   x++;
}

   // Sum array3[] with a do { } while() loop and print the result
int y = 0;
do{
   sumc = sumc + threeArrays.array3[y];
      y++;

}while(y<100);

printf("Sum is %ld\n",suma);
printf("Sum is %ld\n", sumb);
printf("Sum is %ld\n", sumc);

}
